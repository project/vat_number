# VAT Number

This module provides a VAT Number field that can be used in general. The VAT
will be validated against the VAT Information Exchange System. Information on
the EU VAT number checking service.

For a full description of the module, visit the
[project page](https://drupal.org/project/vat_number).

To submit bug reports and feature suggestions, or to track changes
[issue queue](https://drupal.org/project/issues/vat_number).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

There is no configuration.
