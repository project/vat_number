<?php

namespace Drupal\vat_number\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Drupal\vat_number\Controller\VatNumberController;

/**
 * Provides a VAT number element.
 *
 * @FormElement("vat_number")
 */
class VatNumber extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#element_validate'][] = [static::class, 'validateVatNumber'];
    $info['#validate_vies'] = TRUE;
    $info['#fail_if_vies_unavailable'] = TRUE;

    return $info;
  }

  /**
   * Validate the fields and check if the vat number is valid.
   */
  public static function validateVatNumber($element, FormStateInterface $form_state): void {
    $value = $element['#value'];
    if (empty($value)) {
      return;
    }

    $vatController = new VatNumberController($value);
    $valid = $vatController->check($element['#validate_vies'], $element['#fail_if_vies_unavailable']);

    if (!$valid['status']) {
      $form_state->setError($element, $valid['message']);
    }
  }

}
