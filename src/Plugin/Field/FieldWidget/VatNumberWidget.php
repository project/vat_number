<?php

namespace Drupal\vat_number\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'vat_widget' widget.
 *
 * @FieldWidget(
 *   id = "vat_widget",
 *   module = "vat_number",
 *   label = @Translation("VAT Number"),
 *   field_types = {
 *     "vat_number"
 *   }
 * )
 */
class VatNumberWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'validate_vies' => TRUE,
      'fail_if_vies_unavailable' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['validate_vies'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate against VIES database'),
      '#description' => $this->t('If checked, the entered VAT number will be validated against the European VIES VAT database.'),
      '#default_value' => $this->getSetting('validate_vies'),
    ];
    $element['fail_if_vies_unavailable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fail validation if the VIES database is unavailable'),
      '#description' => $this->t('If checked, the validation will always fail if the VIES database is unavailable. Note that either way, the format of the entered VAT number is validated.'),
      '#default_value' => $this->getSetting('fail_if_vies_unavailable'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $validate_vies = $this->getSetting('validate_vies');
    $value = $validate_vies ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('VIES database validation: @value', ['@value' => $value]);

    $fail_if_vies_unavailable = $this->getSetting('fail_if_vies_unavailable');
    $value = $fail_if_vies_unavailable ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Fail if VIES database is unavailable: @value', ['@value' => $value]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element += [
      '#type' => 'vat_number',
      '#default_value' => $items[$delta]->getValue()['value'] ?? NULL,
      '#validate_vies' => $this->getSetting('validate_vies'),
      '#fail_if_vies_unavailable' => $this->getSetting('fail_if_vies_unavailable'),
    ];

    return ['value' => $element];
  }

}
