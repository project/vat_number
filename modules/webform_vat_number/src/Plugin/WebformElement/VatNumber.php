<?php

namespace Drupal\webform_vat_number\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a VAT number element.
 *
 * @WebformElement(
 *   id = "vat_number",
 *   label = @Translation("VAT number"),
 *   description = @Translation("Provides a form element for entering a VAT number."),
 *   category = @Translation("Advanced elements"),
 *   dependencies = {
 *     "vat_number",
 *   }
 * )
 */
class VatNumber extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return parent::getDefaultProperties() + [
      'validate_vies' => TRUE,
      'fail_if_vies_unavailable' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['validation']['validate_vies'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate against VIES database'),
      '#description' => $this->t('If checked, the entered VAT number will be validated against the European VIES VAT database.'),
    ];

    $form['validation']['fail_if_vies_unavailable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fail validation if the VIES database is unavailable'),
      '#description' => $this->t('If checked, the validation will always fail if the VIES database is unavailable. Note that either way, the format of the entered VAT number is validated.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, ?WebformSubmissionInterface $webform_submission = NULL) {
    if ($webform_submission->getWebform()->getOperation() === 'test') {
      $element['#validate_vies'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTestValues(array $element, WebformInterface $webform, array $options = []) {
    return [
      'ATU99999999',
      'BE0999999999',
      'BG999999999',
      'CY99999999L',
      'CZ99999999',
      'CZ999999999',
      'DE999999999',
      'DK99999999',
      'EE999999999',
      'EL999999999',
      'ESX9999999X',
      'FI99999999',
      'FRXX999999999',
      'GB999999999',
      'GB999999999999',
      'GBGD999',
      'HR99999999999',
      'HU99999999',
      'IE9S99999L',
      'IT99999999999',
      'LT999999999',
      'LU99999999',
      'LV99999999999',
      'MT99999999',
      'NL999999999B99',
      'PL9999999999',
      'PT999999999',
      'RO999999999',
      'SE999999999999',
      'SI99999999',
      'SK9999999999',
    ];
  }

}
