<?php

namespace Drupal\Tests\vat_number\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Mostly just tests that we can enable the module.
 *
 * @group vat_number
 */
class ModuleWorksTest extends BrowserTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['vat_number'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that module is enabled.
   */
  public function testThatModuleIsEnabled() {
    $this->assertTrue($this->container->get('module_handler')->moduleExists('vat_number'), 'VAT Number module is installed.');
  }

}
